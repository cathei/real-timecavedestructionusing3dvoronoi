﻿using System;
using UnityEngine;


//Double accuaraccy vector
public struct DoubleVector : System.IEquatable<DoubleVector>
{
    private double _x, _y, _z;
    public double x
    {
        get
        {
            return _x;
        }
        set
        {
            _x = value;
        }
    }
    public double y
    {
        get
        {
            return _y;
        }
        set
        {
            _y = value;
        }
    }
    public double z
    {
        get
        {
            return _z;
        }
        set
        {
            _z = value;
        }
    }

    public Vector3 vector3
    {
        get
        {
            return new Vector3((float)_x, (float)_y, (float)_z);
        }
    }

    public double sqrMagnitude
    {
        get
        {
            return _x * _x + _y * _y + _z * _z;
        }
    }

    public DoubleVector normalized
    {
        get
        {
            return this / Math.Sqrt(sqrMagnitude);
        }
    }

    public double magnitude
    {
        get
        {
            return Math.Sqrt(sqrMagnitude);
        }
    }

    public DoubleVector(double x, double y, double z)
    {
        this._x = x;
        this._y = y;
        this._z = z;
    }

    public DoubleVector(double x, double y)
    {
        this._x = x;
        this._y = y;
        this._z = 0;
    }

    public static DoubleVector Cross(DoubleVector lhs, DoubleVector rhs)
    {
        return new DoubleVector(lhs._y * rhs._z - lhs._z * rhs._y, lhs._z * rhs._x - lhs._x * rhs._z, lhs._x * rhs._y - lhs.y * rhs._x);
    }

    public static double Dot(DoubleVector lhs, DoubleVector rhs)
    {
        return lhs._x * rhs._x + lhs._y * rhs._y + lhs._z * rhs._z;
    }

    public static DoubleVector operator +(DoubleVector v1, DoubleVector v2)
    {
        return new DoubleVector(v1._x + v2._x, v1._y + v2._y, v1._z + v2._z);
    }

    public static DoubleVector operator -(DoubleVector v1, DoubleVector v2)
    {
        return new DoubleVector(v1._x - v2._x, v1._y - v2._y, v1._z - v2._z);
    }

    public static DoubleVector operator /(DoubleVector v, double a)
    {
        return new DoubleVector(v._x / a, v._y / a, v._z / a);
    }

    public static DoubleVector operator *(double a, DoubleVector v)
    {
        return new DoubleVector(v._x * a, v._y * a, v._z * a);
    }

    public static DoubleVector operator *(DoubleVector v, double a)
    {
        return new DoubleVector(v._x * a, v._y * a, v._z * a);
    }

    public bool Equals(DoubleVector other)
    {
        return (this - other).sqrMagnitude < 0.001;
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
