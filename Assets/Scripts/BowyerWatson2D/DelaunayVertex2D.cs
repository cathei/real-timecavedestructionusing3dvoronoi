﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelaunayVertex2D {

    public static int idCounter = 0;
    public DoubleVector loc;

    private List<Triangle> connectedTriangles;

    private int id;
    public DelaunayVertex2D(DoubleVector loc)
    {
        id = idCounter++;
        this.loc = loc;
        this.connectedTriangles = new List<Triangle>();
    }

    public DelaunayVertex2D(double x, double y, double z)
    {
        id = idCounter++;
        this.loc = new DoubleVector(x, y, z);
        this.connectedTriangles = new List<Triangle>();
    }

    public void AddIncidentTetrahedron(Triangle t)
    {
        connectedTriangles.Add(t);
    }

    public void RemoveIncidentTetrahedron(Triangle t)
    {
        connectedTriangles.Remove(t);
    }

    public static bool operator ==(DelaunayVertex2D x, DelaunayVertex2D y)
    {
        return x.id == y.id;
    }
    public static bool operator !=(DelaunayVertex2D x, DelaunayVertex2D y)
    {
        return !(x == y);
    }

    public override bool Equals(object obj)
    {
        return id == ((DelaunayVertex2D)obj).id;
    }

    public override int GetHashCode()
    {
        return id;
    }
}
